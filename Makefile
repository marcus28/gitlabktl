VERSION ?= $(shell git describe --tags)

GO_LDFLAGS := -extldflags "-static"
GO_LDFLAGS += -X main.version=$(VERSION)
GO_LDFLAGS += -w -s

CODECLIMATE_FORMAT ?= text

.PHONY: test
test:
	go test ./...

.PHONY: build
build: clean
	GOARCH=amd64 GOOS=linux CGO_ENABLED=0 go build -ldflags '$(GO_LDFLAGS)' -o gitlabktl

.PHONY: clean
clean:
	rm -f gitlabktl

.PHONY: image
image: build
	docker build -t registry.gitlab.com/gitlab-org/gitlabktl:dev .

.PHONY: lint
lint:
	docker pull registry.gitlab.com/nolith/codeclimate-gocyclo > /dev/null
	docker tag registry.gitlab.com/nolith/codeclimate-gocyclo codeclimate/codeclimate-gocyclo > /dev/null
	docker run --rm --env CODECLIMATE_CODE="$(shell pwd)" \
        --volume "$(shell pwd)":/code \
        --volume /var/run/docker.sock:/var/run/docker.sock \
        --volume /tmp/cc:/tmp/cc \
        codeclimate/codeclimate:0.83.0 analyze --dev -f ${CODECLIMATE_FORMAT}
