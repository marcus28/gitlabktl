package serverless

import (
	"context"
)

// Deployer is an interface that we use to perform a serverless deployment
type Deployer interface {
	DeployDryRun() (string, error)
	Deploy(ctx context.Context) (string, error)
}
