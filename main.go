package main

import (
	"fmt"
	"os"

	"gitlab.com/gitlab-org/gitlabktl/app"
	"gitlab.com/gitlab-org/gitlabktl/logger"
)

var version = "unknown"

func main() {
	fmt.Println("Welcome to gitlabktl tool")

	app := app.NewApp(version)
	app.RegisterCommands()
	err := app.Run(os.Args)

	if err != nil {
		logger.Fatal(err)
	}
}
