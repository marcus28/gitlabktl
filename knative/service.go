package knative

import (
	tmservice "github.com/triggermesh/tm/pkg/resources/service"
)

// Service describes a basic Knative service that can be deployed to a Knative
// cluster and served by Knative Serving
type Service struct {
	Name        string
	Image       string
	Namespace   string
	Secrets     []string
	Labels      []string
	Envs        map[string]string
	Annotations map[string]string
}

type tmService struct {
	tmservice.Service
}

func newTriggermeshService(service Service, cluster Cluster) (*tmService, error) {
	tmservice := &tmService{
		Service: tmservice.Service{
			Name:        service.Name,
			Namespace:   service.Namespace,
			Source:      service.Image,
			EnvSecrets:  service.Secrets,
			Labels:      service.Labels,
			Annotations: service.Annotations,
		},
	}

	if len(tmservice.Namespace) == 0 {
		tmservice.Namespace = cluster.Namespace()
	}

	for env, value := range service.Envs {
		tmservice.Env = append(tmservice.Env, env+":"+value)
	}

	return tmservice, nil
}
