package knative

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewTriggermeshService(t *testing.T) {
	WithMockCluster(func(cluster *MockCluster) {
		cluster.On("Namespace").Return("my-configured-namespace").Once()

		t.Run("when service has namespace defined", func(t *testing.T) {
			service := Service{
				Name:        "my-service",
				Namespace:   "my-namespace",
				Image:       "registry.gitlab.com/my/source",
				Secrets:     []string{"my-secret"},
				Labels:      []string{"my-label"},
				Envs:        map[string]string{"my-env": "my-env-value"},
				Annotations: map[string]string{"desc": "my-annotation"},
			}

			tmservice, err := newTriggermeshService(service, cluster)
			require.NoError(t, err)

			assert.Equal(t, service.Name, tmservice.Name)
			assert.Equal(t, service.Namespace, tmservice.Namespace)
			assert.Equal(t, service.Image, tmservice.Source)
			assert.Equal(t, service.Secrets, tmservice.EnvSecrets)
			assert.Equal(t, service.Labels, tmservice.Labels)
			assert.Equal(t, service.Annotations, tmservice.Annotations)
			assert.Equal(t, []string{"my-env:my-env-value"}, tmservice.Env)
		})

		t.Run("when service does not have namespace defined", func(t *testing.T) {
			service := Service{Name: "my-service"}

			tmservice, err := newTriggermeshService(service, cluster)
			require.NoError(t, err)

			assert.Equal(t, service.Name, tmservice.Name)
			assert.Equal(t, "my-configured-namespace", tmservice.Namespace)
		})
	})
}
