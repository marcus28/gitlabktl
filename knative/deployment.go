package knative

import (
	"github.com/pkg/errors"

	"gitlab.com/gitlab-org/gitlabktl/serverless"
)

type Deployer serverless.Deployer

// We currently have both DeployFunctions() and DeployApplications() because
// `tm` tool deploys them differently.

func NewFunctionsDeployer(manifest serverless.Manifest, config Config) (Deployer, error) {
	cluster, err := NewCluster(config)
	if err != nil {
		return nil, errors.Wrap(err, "could not define a deployment cluster")
	}

	functions := manifest.ToFunctions()

	return &Functions{
		functions: functions,
		cluster:   cluster,
		namespace: config.Namespace(),
		registry:  config.Registry()}, nil
}

func NewApplicationDeployer(service Service, config Config) (Deployer, error) {
	cluster, err := NewCluster(config)
	if err != nil {
		return nil, errors.Wrap(err, "could not define a deployment cluster")
	}

	return &Application{
		service:  service,
		cluster:  cluster,
		registry: config.Registry()}, nil
}
