package runtime

import (
	"context"
	"path"
	"regexp"

	"github.com/gosimple/slug"
	"github.com/pkg/errors"

	"gitlab.com/gitlab-org/gitlabktl/logger"
)

const template = "Dockerfile.template"
const gitlabRuntimePrefix = "https://gitlab.com/gitlab-org/serverless/"

var gitlabRuntime *regexp.Regexp = regexp.MustCompile(`^gitlab/`)

type GeneratedRuntime struct {
	Details
}

// Build runtime files and persist them in CodeDirectory
func (runtime GeneratedRuntime) Build(ctx context.Context) error {
	address := runtime.RuntimeAddress()

	if len(address) == 0 {
		panic("invalid runtime fabricated")
	}

	logger.WithField("runtime", runtime.DockerfilePath()).
		Info("preparing a generated runtime")

	repository := NewRepository(address)
	dockerfile, err := repository.ReadFile(template)

	if err != nil {
		logger.WithField("runtime", address).
			WithError(err).Warn("could not read Dockerfile template")

		return errors.Wrap(err, "could not read Dockerfile template")
	}

	template := Template{
		Filename:   runtime.DockerfilePath(),
		Attributes: runtime.Details,
		Contents:   dockerfile,
	}

	err = template.Write()
	if err != nil {
		logger.WithField("template", runtime.DockerfilePath()).
			WithError(err).Warn("could not write a runtime template")

		return errors.Wrap(err, "could not write a runtime template")
	}

	err = repository.WriteFiles(runtime.CodeDirectory)
	if err != nil {
		logger.WithField("runtime", runtime.RuntimeAddress).
			WithError(err).Warn("could not write runtime context files")

		return errors.Wrap(err, "could not write runtime context files")
	}

	return nil
}

func (runtime GeneratedRuntime) BuildDryRun() (string, error) {
	address := runtime.RuntimeAddress()

	if len(address) == 0 {
		return "generated runtime error", errors.New("invalid runtime fabrication")
	}

	logger.WithField("runtime", runtime.DockerfilePath()).
		Info("preparing a generated runtime (dry-run)")

	return "preparing a generated runtime", nil
}

func (runtime GeneratedRuntime) Slug() string {
	return slug.Make(runtime.FunctionName)
}

func (runtime GeneratedRuntime) DockerfilePath() string {
	filename := "Dockerfile." + runtime.Slug()

	return path.Join(runtime.CodeDirectory, filename)
}

func (runtime GeneratedRuntime) RuntimeAddress() string {
	// If the runtime is a gitlab maintained runtime, we expand the address to
	// a full URL to our repository of serverless runtimes.

	return gitlabRuntime.ReplaceAllLiteralString(runtime.FunctionRuntime, gitlabRuntimePrefix)
}
