package runtime

import (
	"github.com/stretchr/testify/mock"
	"os"
	"time"
)

type MockClient struct {
	mock.Mock
}

func (client *MockClient) ReadFile(path string) (string, error) {
	args := client.Called(path)

	return args.String(0), args.Error(1)
}

func (client *MockClient) WriteFiles(directory string) error {
	args := client.Called(directory)

	return args.Error(0)
}

func (client *MockClient) ListFiles(path string) ([]os.FileInfo, error) {
	args := client.Called(path)

	return args.Get(0).([]os.FileInfo), args.Error(1)
}

type MockFileInfo struct {
	mock.Mock
}

func (file *MockFileInfo) Name() string {
	args := file.Called()

	return args.String(0)
}

func (file *MockFileInfo) Size() int64 {
	panic("implement me")
}

func (file *MockFileInfo) Mode() os.FileMode {
	panic("implement me")
}

func (file *MockFileInfo) ModTime() time.Time {
	panic("implement me")
}

func (file *MockFileInfo) IsDir() bool {
	panic("implement me")
}

func (file *MockFileInfo) Sys() interface{} {
	panic("implement me")
}
