package runtime

import (
	"context"
	"path"

	"gitlab.com/gitlab-org/gitlabktl/logger"
)

type CustomRuntime struct {
	Details
}

// Build runtime, in case of a custom runtime just log the action
func (runtime CustomRuntime) Build(ctx context.Context) error {
	logger.WithField("runtime", runtime.DockerfilePath()).
		Info("using a custom Dockerfile runtime")

	return nil
}

// Build runtime DryRun
func (runtime CustomRuntime) BuildDryRun() (string, error) {
	logger.WithField("runtime", runtime.DockerfilePath()).
		Info("using a custom Dockerfile runtime (DryRun)")

	return "using a custom runtime", nil
}

// Return an expected path to a custom Dockerfile
func (runtime CustomRuntime) DockerfilePath() string {
	return path.Join(runtime.CodeDirectory, "Dockerfile")
}
