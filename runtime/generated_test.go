package runtime

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
)

func TestGeneratedRuntimeBuild(t *testing.T) {
	details := Details{
		FunctionName:    "my func",
		FunctionRuntime: "some/address",
		CodeDirectory:   "my/function/",
	}

	client := new(MockClient)
	defer client.AssertExpectations(t)

	client.On("ReadFile", "Dockerfile.template").
		Return("FROM scratch '{{.FunctionName}}'", nil).Once()
	client.On("WriteFiles", "my/function/").Return(nil).Once()

	newRepositoryClient = func(l Location) (Client, error) { return client, nil }

	fs.WithTestFs(func() {
		runtime := GeneratedRuntime{Details: details}
		err := runtime.Build(context.Background())
		require.NoError(t, err)

		dockerfile, err := fs.ReadFile("my/function/Dockerfile.my-func")
		require.NoError(t, err)

		assert.Equal(t, "FROM scratch 'my func'", string(dockerfile))
	})
}

func TestGeneratedRuntimeBuildDryRun(t *testing.T) {
	t.Run("when no runtime address is provided", func(t *testing.T) {
		runtime := GeneratedRuntime{Details{
			FunctionName:  "my func",
			CodeDirectory: "my/function/",
		}}

		summary, err := runtime.BuildDryRun()

		assert.Equal(t, "generated runtime error", summary)
		assert.Equal(t, "invalid runtime fabrication", err.Error())
	})

	t.Run("when runtime address is provided", func(t *testing.T) {
		runtime := GeneratedRuntime{Details{
			FunctionName:    "my func",
			FunctionRuntime: "my/runtime/address",
			CodeDirectory:   "my/function/",
		}}

		summary, err := runtime.BuildDryRun()

		assert.Equal(t, "preparing a generated runtime", summary)
		assert.NoError(t, err)
	})
}

func TestGeneratedRuntimeSlug(t *testing.T) {
	runtime := GeneratedRuntime{Details: Details{FunctionName: "my-ąłę-func 1"}}

	assert.Equal(t, "my-ale-func-1", runtime.Slug())
}

func TestGeneratedRuntimeDockerfilePath(t *testing.T) {
	runtime := GeneratedRuntime{Details: Details{
		FunctionRuntime: "gitlab.com/some/runtime",
		FunctionName:    "my func ąłę",
	}}
	assert.Equal(t, "Dockerfile.my-func-ale", runtime.DockerfilePath())

	runtime = GeneratedRuntime{Details: Details{
		FunctionRuntime: "gitlab.com/some/runtime",
		FunctionName:    "my func",
		CodeDirectory:   "echo/",
	}}
	assert.Equal(t, "echo/Dockerfile.my-func", runtime.DockerfilePath())
}

func TestRuntimeAddress(t *testing.T) {
	t.Run("when runtime is specified as a URL", func(t *testing.T) {
		runtime := GeneratedRuntime{Details: Details{
			FunctionRuntime: "https://gitlab.com/gitlab-org/serverless/runtimes/ruby",
		}}

		assert.Equal(t, "https://gitlab.com/gitlab-org/serverless/runtimes/ruby", runtime.RuntimeAddress())
	})

	t.Run("when runtime is specified as a short path", func(t *testing.T) {
		runtime := GeneratedRuntime{Details: Details{
			FunctionRuntime: "gitlab/runtimes/ruby",
		}}

		assert.Equal(t, "https://gitlab.com/gitlab-org/serverless/runtimes/ruby", runtime.RuntimeAddress())
	})
}
