package runtime

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	t.Run("when runtime is specified", func(t *testing.T) {
		runtime := New(Details{FunctionRuntime: "example.com/runtime"})
		assert.IsType(t, GeneratedRuntime{}, runtime)
	})

	t.Run("when runtime is not specified", func(t *testing.T) {
		assert.IsType(t, CustomRuntime{}, New(Details{FunctionName: "my-function"}))
	})

	t.Run("when runtime has a prefix of openfaas", func(t *testing.T) {
		assert.IsType(t, OpenfaasRuntime{}, New(Details{FunctionRuntime: "openfaas/classic/ruby"}))
	})
}

func TestImage(t *testing.T) {
	t.Run("when details.ResultingImage is present", func(t *testing.T) {
		runtime := New(Details{ResultingImage: "serverless-function-image", FunctionName: "serverless-function"})
		assert.Equal(t, "serverless-function-image", runtime.Image())
	})

	t.Run("when details.ResultingImage is blank", func(t *testing.T) {
		runtime := New(Details{FunctionName: "serverless-function"})
		assert.Equal(t, "serverless-function", runtime.Image())
	})
}
