package runtime

import (
	"context"
	"strings"
)

type Runtime interface {
	BuildDryRun() (string, error)
	Build(ctx context.Context) error
	Directory() string
	Image() string
	DockerfilePath() string
}

type Details struct {
	FunctionName    string
	FunctionFile    string
	FunctionHandler string
	FunctionRuntime string
	CodeDirectory   string
	ResultingImage  string
}

// Creates a new runtime using Runtime Details.
//
// If a runtime contains a runtime address it means that this is a runtime we
// need to generate. Otherwise it is mostly likely the CustomRuntime, meaning
// that there should be Dockerfile provided by a user.
func New(details Details) Runtime {
	switch {
	case strings.HasPrefix(details.FunctionRuntime, openfaasPrefix):
		return OpenfaasRuntime{Details: details}
	case len(details.FunctionRuntime) > 0:
		return GeneratedRuntime{Details: details}
	default:
		return CustomRuntime{Details: details}
	}
}

func (details Details) Directory() string {
	return details.CodeDirectory
}

func (details Details) Image() string {
	if len(details.ResultingImage) == 0 {
		return details.FunctionName
	}

	return details.ResultingImage
}
