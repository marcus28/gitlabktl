package runtime

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDockerfilePath(t *testing.T) {
	runtime := CustomRuntime{Details: Details{FunctionName: "my func"}}
	assert.Equal(t, "Dockerfile", runtime.DockerfilePath())

	runtime = CustomRuntime{Details: Details{CodeDirectory: "echo"}}
	assert.Equal(t, "echo/Dockerfile", runtime.DockerfilePath())
}

func TestCustomRuntimeBuild(t *testing.T) {
	runtime := CustomRuntime{Details{
		FunctionName:  "my func",
		CodeDirectory: "my/function/",
	}}

	err := runtime.Build(context.Background())

	assert.NoError(t, err)
}

func TestCustomRuntimeBuildDryRun(t *testing.T) {
	runtime := CustomRuntime{Details{
		FunctionName:  "my func",
		CodeDirectory: "my/function/",
	}}

	summary, err := runtime.BuildDryRun()

	assert.Equal(t, "using a custom runtime", summary)
	assert.NoError(t, err)
}
